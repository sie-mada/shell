import { MaterialFileInputModule } from 'ngx-material-file-input';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { A11yModule } from '@angular/cdk/a11y';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';
import { EmbedDashboardComponent } from './modules/dashboard/embed-dashboard/embed-dashboard.component';
import { IndicatorTreeComponent } from './modules/dash-tree/indicator-tree.component';
import { ImprintComponent } from './modules/imprint/imprint.component';
import { LoginComponent } from './modules/login/login.component';
import { UserSettingsComponent } from './modules/user-settings/user-settings.component';
import { AddSectionComponent } from './modules/dash-tree/add-section/add-section.component';
import { ConfirmDialogComponent } from './modules/dash-tree/confirm-dialog/confirm-dialog.component';
import { DashControlsComponent } from './modules/dashboard/dash-controls/dash-controls.component';
import { CommentsDisplayComponent } from './modules/dashboard/dash-controls/comments-display/comments-display.component';
import { UserAdminComponent } from './modules/user-admin/user-admin.component';
import { EditUserComponent } from './modules/user-admin/edit-user/edit-user.component';
import { AuthorizeDirective } from './modules/login/authorize.directive';
import { DataAdminComponent } from './modules/data-admin/data-admin.component';
import { JoomlaFixerComponent } from './modules/tasks/joomla-fixer/joomla-fixer.component';
import { ThemeSwitcherComponent } from './modules/theme-switcher/theme-switcher.component';
import { ImportProgressComponent } from './modules/data-admin/import-progress/import-progress.component';
import { ImportsManagementComponent } from './modules/user-admin/imports-management/imports-management.component';
import { DashboardHomeComponent } from './modules/dashboard-home/components/dashboard-home.component';
import { DashboardSectionComponent } from './modules/dashboard-home/components/dashboard-section/dashboard-section.component';
import { MaterialElevationDirective } from './modules/dashboard-home/directives/material-elevation.directive';
import { DashboardIndicatorComponent } from './modules/dashboard-home/components/dashboard-indicator/dashboard-indicator.component';
import { EmbedDashboardIndicatorComponent } from './modules/dashboard-home/components/dashboard-indicator/embed-dashboard-indicator/embed-dashboard-indicator.component';
import { AddSectionDialogComponent } from './modules/dashboard-home/components/add-section-dialog/add-section-dialog.component';
import { KpiChartsComponent } from './modules/data-admin/kpi-charts/kpi-charts.component';
import { ChartsModule } from 'ng2-charts';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { MaterialModule } from './material.module';
import { DataMappingTableComponent } from './modules/data-admin/data-mapping-table/data-mapping-table.component';
import { ImportsManagementDetailsComponent } from './modules/user-admin/imports-management/imports-management-details/imports-management-details.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { FileStructureViewerComponent } from './modules/user-admin/imports-management/imports-management-details/file-structure-viewer/file-structure-viewer.component';
import { RejectImportDialogComponent } from './modules/user-admin/imports-management/imports-management-details/reject-import-dialog/reject-import-dialog.component';
import { RejectInfoHistoryViewerComponent } from './modules/user-admin/imports-management/imports-management-details/reject-info-history-viewer/reject-info-history-viewer.component';
import { FilterPanelComponent } from './modules/user-admin/imports-management/imports-management-details/filter-panel/filter-panel.component';
import { CloseDialogComponent } from './shared/components/close-dialog/close-dialog.component';

@NgModule({
  declarations: [
    AddSectionComponent,
    AppComponent,
    ConfirmDialogComponent,
    CloseDialogComponent,
    EmbedDashboardComponent,
    EmbedDashboardIndicatorComponent,
    HomeComponent,
    ImprintComponent,
    IndicatorTreeComponent,
    LoginComponent,
    UserSettingsComponent,
    DashControlsComponent,
    CommentsDisplayComponent,
    UserAdminComponent,
    EditUserComponent,
    AuthorizeDirective,
    MaterialElevationDirective,
    DataAdminComponent,
    JoomlaFixerComponent,
    ThemeSwitcherComponent,
    ImportProgressComponent,
    ImportsManagementComponent,
    ImportsManagementDetailsComponent,
    DashboardComponent,
    DashboardHomeComponent,
    DashboardSectionComponent,
    DashboardIndicatorComponent,
    AddSectionDialogComponent,
    KpiChartsComponent,
    DataMappingTableComponent,
    FileStructureViewerComponent,
    RejectImportDialogComponent,
    RejectInfoHistoryViewerComponent,
    FilterPanelComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    CloseDialogComponent,
    DashControlsComponent,
    AddSectionDialogComponent,
    RejectImportDialogComponent,
  ],
  imports: [
    A11yModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    DragDropModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MaterialFileInputModule,
    ChartsModule,
    NgxJsonViewerModule
  ],
  providers: [ThemeSwitcherComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
