import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from './core/services/user.service';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ThemeSwitcherComponent } from './modules/theme-switcher/theme-switcher.component';

@Component({
  selector: 'sie-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'shell';
  size?: string;
  breakpoints = Breakpoints
  isAuthenticated = false;
  isHome = false;

  private breakPointSubscription?: Subscription;

  constructor(
    breakpointObserver: BreakpointObserver,
    private readonly router: Router,
    private readonly userService: UserService,
    public themeSwitch: ThemeSwitcherComponent
  ) {
    this.breakPointSubscription = breakpointObserver
      .observe(
        [Breakpoints.XSmall,
        Breakpoints.Small,
        Breakpoints.Medium,
        Breakpoints.Large,
        Breakpoints.XLarge
        ])
      .subscribe((state: BreakpointState | any) => {
        if (state.breakpoints[Breakpoints.XSmall]) {
          this.size = Breakpoints.XSmall;
        }
        if (state.breakpoints[Breakpoints.Small]) {
          this.size = Breakpoints.Small;
        }
        if (state.breakpoints[Breakpoints.Medium]) {
          this.size = Breakpoints.Medium;
        }
        if (state.breakpoints[Breakpoints.Large]) {
          this.size = Breakpoints.Large;
        }
        if (state.breakpoints[Breakpoints.XLarge]) {
          this.size = Breakpoints.XLarge;
        }
      });

    router.events
      .pipe(
        filter(event => event instanceof ActivationEnd)
      )
      .subscribe(event => {
        const routeConfig = (event as ActivationEnd).snapshot.routeConfig;
        this.isHome = !!routeConfig && routeConfig.path === 'accueil';
      });
  }

  ngOnInit() {
    this.themeSwitch.setDefaultTheme()
    this.userService.user.subscribe(user => {
      if (user.isAuthenticated) {
        this.isAuthenticated = true;
      } else {
        this.isAuthenticated = false;
        this.router
          .navigate(['/enregistrer'])
          .catch(
            error => console.error('La navigation à la page d\'enregistrement a échoué:', error)
          );
      }
    });
  }

  ngOnDestroy() {
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
      this.breakPointSubscription = undefined;
    }
  }

  logout() {
    this.userService.logout();
    this.isAuthenticated = false;
  }
}
