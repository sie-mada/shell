import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { ImprintComponent } from './modules/imprint/imprint.component';
import { LoginComponent } from './modules/login/login.component';
import { UserSettingsComponent } from './modules/user-settings/user-settings.component';
import { UserAdminComponent } from './modules/user-admin/user-admin.component';
import { EditUserComponent } from './modules/user-admin/edit-user/edit-user.component';
import { DataAdminComponent } from './modules/data-admin/data-admin.component';
import { JoomlaFixerComponent } from './modules/tasks/joomla-fixer/joomla-fixer.component';
import { ImportsManagementComponent } from './modules/user-admin/imports-management/imports-management.component';
import { DashboardHomeComponent } from './modules/dashboard-home/components/dashboard-home.component';
import { DashboardSectionComponent } from './modules/dashboard-home/components/dashboard-section/dashboard-section.component';
import { DashboardIndicatorComponent } from './modules/dashboard-home/components/dashboard-indicator/dashboard-indicator.component';
import { IndicatorTreeComponent } from './modules/dash-tree/indicator-tree.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { ImportsManagementDetailsComponent } from './modules/user-admin/imports-management/imports-management-details/imports-management-details.component';
import { CanDeactivateGuard } from './core/guards/can-deactivate.guard';

const routes: Routes = [
  {
    path: 'enregistrer',
    component: LoginComponent
  },
  {
    path: 'accueil',
    component: HomeComponent
  },
  {
    path: 'mentions-legales',
    component: ImprintComponent
  },
  {
     path: 'tableau-indicateur/:sec/:dash',
     component: DashboardIndicatorComponent
   },
  {
    path: 'tableau',
    component: DashboardHomeComponent
  },
  {
    path: 'tableau-section/:sec',
    component: DashboardSectionComponent
  },
  {
    path: 'tableau/gérer',
    component: IndicatorTreeComponent
  },
  {
    path: 'tableau/gérer/:dash',
    component: DashboardComponent
  },
  {
    path: 'mon-compte',
    component: UserSettingsComponent
  },
  {
    path: 'utilisateurs',
    component: UserAdminComponent
  },
  {
    path: 'utilisateurs/:username',
    component: EditUserComponent
  },
  {
    path: 'donnees/televerser',
    component: DataAdminComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'donnees/approuver',
    component: ImportsManagementComponent
  },
  {
    path: 'donnees/approuver/:id/:step',
    component: ImportsManagementDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'tache/correcture-joomla',
    component: JoomlaFixerComponent
  },
  {
    path: '',
    redirectTo: 'enregistrer',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
