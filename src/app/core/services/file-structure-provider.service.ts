import { Injectable } from '@angular/core';
import { ImportType } from 'src/app/shared/models/ImportType';
import { FileStructureObject, EVOLME_FILE_STRUCTURE, EVOLME_FILE_STRUCTURE_FRENCH, ADER_MATRICE_FILE_STRUCTURE, ADER_MATRICE_FILE_STRUCTURE_FRENCH } from 'src/app/shared/models/FileStructure';

@Injectable({
  providedIn: 'root'
})
export class FileStructureProviderService {

  constructor() { }

  getFileStructure(importType: ImportType): FileStructureObject {
    let fileStructure!: FileStructureObject;
    switch (importType.name) {
      case 'evolme':
        fileStructure = {
          FileStructure: JSON.parse(JSON.stringify(EVOLME_FILE_STRUCTURE)),
          FileStructureFrench: JSON.parse(JSON.stringify(EVOLME_FILE_STRUCTURE_FRENCH))
        };
        break;
      case 'ader-matrice':
        fileStructure = {
          FileStructure: JSON.parse(JSON.stringify(ADER_MATRICE_FILE_STRUCTURE)),
          FileStructureFrench: JSON.parse(JSON.stringify(ADER_MATRICE_FILE_STRUCTURE_FRENCH))
        };
        break;
      default:
        break;
    }
    return fileStructure;
  }
}
