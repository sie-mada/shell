import { TestBed } from '@angular/core/testing';

import { FileStructureProviderService } from './file-structure-provider.service';

describe('FileStructureProviderService', () => {
  let service: FileStructureProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileStructureProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
