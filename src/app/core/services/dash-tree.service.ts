import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { omitAll, kebabCase } from 'lodash/fp';
import { Observable, BehaviorSubject, ConnectableObservable } from 'rxjs';
import { UserService } from './user.service';
import { flatMap, map, filter, share, tap, publishReplay, take } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { CoreConfig } from '../../shared/models/core';
import { DashTreeSection, Dashboard, DashTreeChild, isDashTreeSection } from 'src/app/shared/models/Dashboard';
import { User } from 'src/app/shared/models/User';

@Injectable({
  providedIn: 'root'
})
export class DashTreeService {

  private readonly treeSubject = new BehaviorSubject<(DashTreeSection & DashTreeChild)[]>([]);
  readonly treeChanged$ = this.treeSubject.asObservable();
  private dashboards: Observable<Map<string, Dashboard>>;
  private deafultHeaders?: HttpHeaders;

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient,
    private readonly auth: UserService
  ) {
    this.dashboards = this.treeSubject.pipe(
      map(tree => this.buildDashboardIndex(tree)),
      filter(dashboardMap => !!dashboardMap.size),
      share()
    );
    this.auth.user.subscribe(user => {
      if (user.isAuthenticated) {
        this.deafultHeaders = this.buildDefaultHeaders(user);
        this.rebuildTree();
      } else {
        this.deafultHeaders = new HttpHeaders();
        this.treeSubject.next([]);
      }
    });
  }

  private buildDefaultHeaders(user: User): HttpHeaders {
    return new HttpHeaders({
      Authorization: `Basic ${btoa(user.username + ':' + user.password)}`
    });
  }

  addSection(parent: string | undefined, identifier: string, extraTitle?: string) {
    const title = extraTitle || identifier;
    const name = (parent ? `${parent}.` : '') + kebabCase(identifier);
    const observable = this.configService.getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.put<DashTreeSection>(
          `${config.shell}/dashboard/section/${name}`,
          { name, title, parent },
          { headers: this.deafultHeaders }
        )),
        tap(() => this.rebuildTree()),
        publishReplay(1)
      ) as ConnectableObservable<DashTreeSection>;

    observable.connect();

    return observable;
  }

  deleteSection(name: string) {
    const observable = this.configService.getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.delete(`${config.shell}/dashboard/section/${name}`, { headers: this.deafultHeaders })),
        tap(() => this.rebuildTree()),
        publishReplay(1)
      ) as ConnectableObservable<any>;

    observable.connect();

    return observable;
  }

  getIndicator(name: string): Observable<Dashboard | unknown> {
    return this.dashboards
      .pipe(
        map(dashboardMap => dashboardMap.get(name)),
        filter(Boolean)
      );
  }

  rebuildTree() {
    this.getTreeData()
      .pipe(
        take(1)
      )
      .subscribe(
        tree => this.treeSubject.next(<(DashTreeSection & DashTreeChild)[]>tree),
        err => this.treeSubject.error(err)
      );
  }

  private getTreeData(): Observable<(DashTreeSection & DashTreeChild)[] | unknown> {
    return this.configService.getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<DashTreeSection[]>(`${config.shell}/dashboard`, { headers: this.deafultHeaders })),
        map(treeRoot => setNodesParents(treeRoot)),
        filter(Boolean)
      );
  }

  private buildDashboardIndex(tree: DashTreeSection[]): Map<string, Dashboard> {
    if (!tree) {
      return new Map();
    }

    function reduceSection(index: Map<string, Dashboard>, section: DashTreeSection) {
      if (section.indicators) {
        index = section.indicators.reduce(reduceDashboard, index);
      }

      if (section.subSections) {
        index = section.subSections.reduce(reduceSection, index);
      }

      return index;
    }

    function reduceDashboard(index: Map<string, Dashboard>, dashboard: Dashboard) {
      return index.set(dashboard.id, dashboard);
    }

    return tree.reduce(
      reduceSection,
      new Map()
    );
  }

  reassignNode(node: DashTreeSection | Dashboard, newParent: DashTreeSection | null) {
    const nodeEntity = {
      ...omitAll(['indicators', 'subSections', 'parent', 'section'], node),
      section: newParent ? newParent.name : null
    };

    const path = isDashTreeSection(node)
      ? `dashboard/section/${node.id}`
      : `dashboard/${node.adapter}/${node.remoteId}`;

    const observable = this.configService.getSection<CoreConfig>('core')
      .pipe(
        flatMap(
          config => this.http.put<DashTreeSection | Dashboard>(`${config.shell}/${path}`, nodeEntity, { headers: this.deafultHeaders })
        ),
        tap(() => this.rebuildTree()),
        publishReplay(1)
      ) as ConnectableObservable<DashTreeSection | Dashboard>;

    observable.connect();

    return observable;
  }
}

function setNodesParents(nodes: DashTreeSection[], parent: DashTreeSection | null = null): DashTreeSection[] {
  return nodes.map(
    node => {
      const mapped = {
        ...node,
        parent
      };
      mapped.subSections = setNodesParents(node.subSections || [], mapped);
      mapped.indicators = (node.indicators || []).map(indicator => setDashboardParent(indicator, mapped));

      return mapped;
    }
  );
}

function setDashboardParent(node: Dashboard, parent: DashTreeSection | null) {
  return {
    ...node,
    parent
  };
}
