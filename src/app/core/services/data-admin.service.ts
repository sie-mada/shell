import { Injectable } from '@angular/core';
import { Observable, of, timer } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { CoreConfig } from '../../shared/models/core';
import { flatMap, map, catchError, delay, tap, retry, switchMap, share } from 'rxjs/operators';
import { UserService } from './user.service';
import { ImportType } from 'src/app/shared/models/ImportType';
import { ImportStatus } from 'src/app/shared/models/ImportStatus';
import { ImportPreResultList } from 'src/app/shared/models/ImportPreResultList';
import { ImportHistoryElement } from 'src/app/shared/models/ImportHistoryElement';
import { FileStructureObject } from 'src/app/shared/models/FileStructure';
import { ImportDetail } from 'src/app/shared/models/ImportDetail';


@Injectable({
  providedIn: 'root'
})
export class DataAdminService {

  constructor(
    private readonly config: ConfigService,
    private readonly http: HttpClient,
    private readonly userService: UserService
  ) { }

  getImporters(): Observable<ImportType[]> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<ImportType[]>(`${config.shell}/data/importer`, { headers: this.userService.authenticationHeader }))
      );
  }

  getCoreConfig(): Observable<CoreConfig> {
    return this.config.getSection<CoreConfig>('core');;
  }

  getImportHistoryList(): Observable<ImportHistoryElement[]> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<ImportHistoryElement[]>(`${config.shell}/data/importer/importhistorylist`, { headers: this.userService.authenticationHeader }))
      );
  }

  getImportDetail(type: string, importId: string): Observable<ImportDetail> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<ImportDetail>(
          `${config.shell}/data/importer/${type}/importdetail/${importId}`,
          { headers: this.userService.authenticationHeader, withCredentials: true }))
      );
  }

  getImportStatus(type: string, id: string): Observable<ImportStatus> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<ImportStatus>(
          `${config.shell}/data/importer/${type}/import/${id}`,
          { headers: this.userService.authenticationHeader, withCredentials: true }))
      );
  }

  upload(type: string, importId: string): Observable<boolean | string> {
    const formData = new FormData();
    formData.append('user', this.userService.currentUser!.username!);
    formData.append('importId', importId);

    let backendUrl: string;

    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        tap(config => backendUrl = config.shell),
        flatMap(config => this.http
          .post<ImportStatus>(
            `${config.shell}/data/importer/${type}/import`,
            formData,
            { headers: this.userService.authenticationHeader }
          )
        ),
        map(status => status.importId),
        flatMap(importId => this.waitForImport(type, importId, backendUrl)),
        tap(result => {
          if (typeof result === 'string') {
            throw new Error(result);
          }
        })
      );
  }

  rejectImport(type: string, importId: string, rejectText: string): Observable<boolean> {
    const formData = new FormData();
    formData.append('user', this.userService.currentUser!.username!);
    formData.append('time', new Date().toISOString().substring(0,19).replace("T"," "));

    formData.append('importId', importId);
    formData.append('rejectText', rejectText);
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .post<boolean>(
            `${config.shell}/data/importer/${type}/reject`,
            formData,
            { headers: this.userService.authenticationHeader }
          )
        )
      );
  }

  importCheck(type: string, file: File, fileStructure: FileStructureObject, checkFile: boolean): Observable<ImportStatus> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('user', this.userService.currentUser!.username!);
    formData.append('fileStructure', JSON.stringify(fileStructure))
    formData.append('checkFile', JSON.stringify(checkFile));

    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .post<ImportStatus>(
            `${config.shell}/data/importer/${type}/importcheck`,
            formData,
            { headers: this.userService.authenticationHeader }
          )
        )
      );
  }

  importPreProcess(type: string, importId: string): Observable<ImportPreResultList> {
    const formData = new FormData();
    formData.append('importId', importId);
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .post<ImportPreResultList>(
            `${config.shell}/data/importer/${type}/importpreprocess`,
            formData,
            { headers: this.userService.authenticationHeader }
          )
        )
      );
  }

  waitForCheckImport(type: string, id: string): Observable<ImportStatus> {
    return timer(0, 500).pipe(
      switchMap(() => this.getImportStatus(type, id)),
      retry(),
      share(),
    )
  }

  private waitForImport(type: string, id: string, backendUrl: string) {
    return of(null).pipe(
      delay(2500),
      flatMap(() => this.http.get<ImportStatus>(
        `${backendUrl}/data/importer/${type}/import/${id}`,
        { headers: this.userService.authenticationHeader, withCredentials: true }
      )),
      catchError(err => (console.error(err), 'L\'importation a échoué')),
      map(result => {
        if (typeof result === 'string') {
          return result;
        }

        if (result.status === 'failed') {
          return 'L\'importation a échoué';
        }

        if (result.status === 'done') {
          return true;
        }

        // result.status === 'running'
        throw new Error();
      }),
      retry()
    );
  }
}
