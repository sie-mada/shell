import { Injectable } from '@angular/core';
import { Observable, ConnectableObservable } from 'rxjs';
import * as moment from 'moment';
import { ConfigService } from './config.service';
import { CoreConfig } from '../../shared/models/core';
import { flatMap, take, map, publishReplay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';
import { DashboardComment } from 'src/app/shared/models/DashboardComment';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient,
    private readonly userService: UserService
  ) { }

  getCommentsFor(adapter: string, remoteId: string): Observable<DashboardComment[]> {
    return this.configService.getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<DashboardComment[]>(
          `${config.shell}/dashboard/${adapter}/${remoteId}/comment`,
          {
            headers: this.userService.authenticationHeader
          }
        )),
        map(data => data || []),
        map(comments => comments.map(comment => ({ ...comment, timestamp: moment(comment.timestamp) })))
      );
  }

  appendCommentTo(adapter: string, remoteId: string, comment: DashboardComment): Observable<DashboardComment[]> {
    const observable = this.configService.getSection<CoreConfig>('core')
      .pipe(
        take(1),
        flatMap(config =>
          this.http.post<DashboardComment[]>(
            `${config.shell}/dashboard/${adapter}/${remoteId}/comment`,
            {
              ...comment,
              timestamp: comment.timestamp.format()
            },
            {
              headers: this.userService.authenticationHeader
            }
          )
        ),
        publishReplay(1)
      ) as ConnectableObservable<DashboardComment[]>;

    observable.connect();

    return observable;
  }
}
