import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { ConfigService } from './config.service';
import { CoreConfig } from '../../shared/models/core';
import { flatMap, map, tap, mapTo } from 'rxjs/operators';
import { isUserSessionRequestMessage } from 'src/app/shared/models/UserSessionRequestMessage';
import { User } from 'src/app/shared/models/User';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userSubject = new BehaviorSubject<User>({ isAuthenticated: false });
  private $currentUser?: User;
  readonly user: Observable<User>;
  private basicCredentials?: string;
  private userSessionExchangeChannel = new BroadcastChannel('userSession');

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient
  ) {
    this.loadSessionUser();
    this.userSubject.subscribe(user => {
      this.$currentUser = user;
      sessionStorage.setItem('user', JSON.stringify(user));
      this.basicCredentials = user.isAuthenticated
        ? btoa(user.username + ':' + user.password)
        : undefined;
    });
    this.user = this.userSubject.asObservable();
  }

  private loadSessionUser() {
    this.userSessionExchangeChannel.addEventListener('message', this.receiveExternalSessionMessage.bind(this));

    const sessionUserJson = sessionStorage.getItem('user');
    if (sessionUserJson) {
      const sessionUser = JSON.parse(sessionUserJson) as User;

      if (sessionUser && sessionUser.isAuthenticated) {
        return this.userSubject.next(sessionUser);
      }
    }

    this.userSessionExchangeChannel.postMessage('getSession');
  }

  private receiveExternalSessionMessage(ev: MessageEvent) {
    const message = ev.data;

    if (typeof message === 'string' && message === 'getSession') {
      if (this.$currentUser && this.$currentUser.isAuthenticated) {
        this.userSessionExchangeChannel.postMessage({ key: 'userSession', session: this.$currentUser });
      }
    }

    if (isUserSessionRequestMessage(message) && !(this.$currentUser && this.$currentUser.isAuthenticated)) {
      this.userSubject.next(message.session);
    }
  }

  get currentUser() {
    return this.$currentUser;
  }

  get authenticationHeader(): HttpHeaders {
    return new HttpHeaders({
      authorization: `Basic ${this.basicCredentials}`
    });
  }

  login(username: string, password: string): Observable<User> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.post<User>(`${config.shell}/login`, { payload: { username, password } })),
        map(u => ({ password, isAuthenticated: true, ...u })),
        tap(u => this.userSubject.next(u))
      );
  }

  update(user: Partial<User>, newPassword?: string): Observable<User> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .put<User>(
            `${config.shell}/user/${user.username}`,
            { ...user, password: newPassword },
            { headers: this.authenticationHeader }
          )
        )
      );
  }

  add(user: User, password: string): Observable<User> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .post<User>(
            `${config.shell}/user`,
            { ...user, password },
            {
              headers: this.authenticationHeader
            }
          )
        )
      );
  }

  delete(username: string): Observable<void> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http
          .delete(`${config.shell}/user/${username}`, { headers: this.authenticationHeader, responseType: 'text' })
        ),
        mapTo(undefined)
      );
  }

  logout() {
    this.userSubject.next({ isAuthenticated: false });
  }

  getUserDetails(username: string): Observable<User> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<User>(`${config.shell}/user/${username}`, { headers: this.authenticationHeader }))
      );
  }

  listUsers(): Observable<User[]> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<User[]>(`${config.shell}/user`, { headers: this.authenticationHeader }))
      );
  }

  listGroups(): Observable<string[]> {
    return this.configService
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<string[]>(`${config.shell}/group`, { headers: this.authenticationHeader }))
      );
  }
}
