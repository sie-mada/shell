import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'sie-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  username?: string;
  oldPassword?: string;
  newPassword?: string;
  repeatPassword?: string;
  errorMessage?: string;
  successMessage?: string;

  constructor(
    private readonly userService: UserService
  ) { }

  ngOnInit() {
    this.userService.user.subscribe(u => {
      this.username = u.username;
    });
  }

  update() {
    if (!this.newPassword || this.newPassword !== this.repeatPassword) {
      this.errorMessage = 'Le nouveau mot de passe n\'est pas en accord avec la repitition';
      return;
    }

    if (this.oldPassword !== (this.userService.currentUser || { password: null }).password) {
      this.errorMessage = 'Le mot de passe présent n\'est pas correct';
      return;
    }

    this.userService.user.subscribe(user => {
      const response = this.userService.update(user, this.newPassword);
      if (typeof response === 'string') {
        console.error(response);
        this.errorMessage = 'Accès non autorisé';
        this.discard();
      } else {
        response.subscribe(data => {
          user.password = data.password;
          this.successMessage = 'Le mot de passe a été changé';
          this.discard();
        }, error => {
          console.error(error);
          this.errorMessage = 'Accès non autorisé';
          this.discard();
        });
      }
    });
  }

  discard() {
    this.oldPassword = this.newPassword = '';
  }
}
