import { Component, OnInit, Input, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'sie-embed-dashboard',
  templateUrl: './embed-dashboard.component.html',
  styleUrls: ['./embed-dashboard.component.scss']
})
export class EmbedDashboardComponent implements OnInit {

  @Input()
  safeUrl: SafeResourceUrl | null = null;

  get url() {
    return this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.safeUrl) || '';
  }

  @Input()
  set url(url: string | SafeResourceUrl) {
    this.safeUrl = typeof url === 'string' ? this.sanitizer.bypassSecurityTrustResourceUrl(url) : url;
  }

  constructor(
    private readonly sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }
}
