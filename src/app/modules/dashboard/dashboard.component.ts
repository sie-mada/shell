import { Component, OnInit, SecurityContext } from '@angular/core';
import { DashTreeService } from '../../core/services/dash-tree.service';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, catchError, publishReplay, tap, take, filter } from 'rxjs/operators';
import { EMPTY, ConnectableObservable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { DashControlsComponent } from './dash-controls/dash-controls.component';
import { Dashboard } from '../../shared/models/Dashboard';

@Component({
  selector: 'sie-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  url$?: ConnectableObservable<SafeUrl>;
  private dashboard?: Dashboard;

  constructor(
    private readonly bottomSheet: MatBottomSheet,
    private readonly dashService: DashTreeService,
    private readonly route: ActivatedRoute,
    private readonly sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.url$ = this.route.paramMap.pipe(
      map(params => params.get('dash')),
      filter(Boolean),
      switchMap(dashId => this.dashService.getIndicator(<string>dashId)),
      tap(dash => (<Dashboard | unknown>this.dashboard) = dash),
      map(dash => this.sanitizer.bypassSecurityTrustResourceUrl((<Dashboard>dash).url)),
      catchError(() => {
        return EMPTY;
      }),
      publishReplay(1)
    ) as ConnectableObservable<SafeUrl>;

    this.url$.connect();
  }

  openBottomSheet() {
    if (!this.url$) {
      return;
    }

    this.url$
      .pipe(
        take(1)
      )
      .subscribe(url =>
        this.bottomSheet.open(
          DashControlsComponent,
          {
            data: {
              dashboard: this.dashboard,
              url: this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, url)
            }
          }
        )
      );
  }
}
