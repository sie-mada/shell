import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Moment } from 'moment';
import * as moment from 'moment';
import { UserService } from 'src/app/core/services/user.service';
import { take, map, tap } from 'rxjs/operators';
import { User } from 'src/app/shared/models/User';
import { Comment } from 'src/app/shared/models/Comment';


@Component({
  selector: 'sie-comments-display',
  templateUrl: './comments-display.component.html',
  styleUrls: ['./comments-display.component.scss']
})
export class CommentsDisplayComponent implements OnInit {

  @Input() comments: Comment[] = [];
  @Output() append: EventEmitter<Comment> = new EventEmitter();

  newComment?: string;

  constructor(
    private readonly userService: UserService
  ) { }

  ngOnInit() {
  }

  formatDate(date: Moment): string {
    if (moment().diff(date, 'days') <= 7) {
      return date.fromNow();
    }

    return date.format('ll');
  }

  appendComment(content: string | undefined) {
    if (!content) {
      return;
    }

    this.userService.user
      .pipe(
        take(1),
        map<User, Comment>(user => ({
          user: user.username || '<none>',
          timestamp: moment(),
          content
        })),
        tap(comment => this.append.emit(comment))
      )
      .subscribe(comment => {
        this.addNewComment(comment);
        this.newComment = '';
      });
  }

  private addNewComment(comment: Comment) {
    this.comments.unshift(comment);
  }
}
