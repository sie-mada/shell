import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { DashTreeService } from '../../core/services/dash-tree.service';
import { CdkDragSortEvent } from '@angular/cdk/drag-drop';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { UserService } from '../../core/services/user.service';
import { IndicatorTreeNode, FlattenedNode, isAppender } from '../../shared/models/IndicatorTreeAppender';
import { DashTreeSection, DashTreeChild, isDashTreeSection, isDashboard, Dashboard } from '../../shared/models/Dashboard';


@Component({
  selector: 'sie-indicator-tree',
  templateUrl: './indicator-tree.component.html',
  styleUrls: ['./indicator-tree.component.scss']
})
export class IndicatorTreeComponent implements OnInit {

  readonly treeControl = new FlatTreeControl<IndicatorTreeNode & FlattenedNode>(node => node.level, node => this.isExpandable(0, node));
  readonly treeDataSource = new MatTreeFlatDataSource<IndicatorTreeNode, IndicatorTreeNode & FlattenedNode>(
    this.treeControl,
    new MatTreeFlattener(
      (node, level) => { (node as any as FlattenedNode).level = level; return node as IndicatorTreeNode & FlattenedNode; },
      node => node.level,
      node => this.isExpandable(0, node),
      node => this.buildChildrenList(node)
    )
  );

  constructor(
    private readonly dialogService: MatDialog,
    private readonly indicatorService: DashTreeService,
    private readonly userService: UserService
  ) { }

  ngOnInit() {
    this.indicatorService.treeChanged$.subscribe(data => {
      const expandedNodes = this.listExpandedNodes();
      this.treeDataSource.data = data;
      this.restoreExpandedState(expandedNodes);
    });
  }

  private listExpandedNodes() {
    if (!this.treeControl.dataNodes) {
      return [];
    }

    return this.treeControl.dataNodes.reduce(
      (set, node) => ({
        ...set,
        ...(this.treeControl.isExpandable(node) && this.treeControl.isExpanded(node) ? { [node.id]: true } : { })
      }),
      {}
    );
  }

  private restoreExpandedState(expanded: Record<string, string>) {
    this.treeControl.dataNodes
      .filter(node => expanded[node.id])
      .forEach(node => this.expandWithParents(node));
  }

  private expandWithParents(node: IndicatorTreeNode & FlattenedNode) {
    this.treeControl.expand(node);
    if (node.parent) {
      this.expandWithParents(node.parent as DashTreeSection & FlattenedNode & DashTreeChild);
    }
  }

  private buildChildrenList(node: IndicatorTreeNode, includeAddender: boolean = true): IndicatorTreeNode[] {
    if (isDashboard(node) || isAppender(node)) {
      return [];
    }

    const currentUser = this.userService.currentUser;
    const userCanAppend = currentUser
      && currentUser.isAuthenticated
      && currentUser.groups
      && currentUser.groups.includes('data_admins');

    const children = [...(node.subSections || []), ...(node.indicators || [])] as IndicatorTreeNode[];
    if (node.id !== 'unassigned' && includeAddender && userCanAppend) {
      children.push({ type: 'sectionAppender', parent: node, id: `${node.id}.__sectionAppender__` });
    }

    return children;
  }

  isExpandable(_: number, node: IndicatorTreeNode): boolean {
    return isDashTreeSection(node); // && !!(node.subSections && node.subSections.length || node.indicators && node.indicators.length);
  }

  isIndicator(_: number, node: IndicatorTreeNode): boolean {
    return isDashboard(node);
  }

  isAppender(_: number, node: IndicatorTreeNode) {
    return isAppender(node);
  }

  canModify(node: DashTreeSection) {
    const currentUser = this.userService.currentUser;
    return node.name !== 'unassigned'
      && currentUser
      && currentUser.isAuthenticated
      && currentUser.groups
      && currentUser.groups.includes('data_admins');
  }

  deleteSection(node: DashTreeSection) {
    this.dialogService
      .open(ConfirmDialogComponent, { data: { action: `supprimer la section « ${node.name} »` }})
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.indicatorService.deleteSection(node.name);
        }
      });
  }

  getNodeId(_: number, node: IndicatorTreeNode) {
    return node.id;
  }

  drop(event: CdkDragSortEvent) {
    const moving = event.item.data;
    if (event.currentIndex === 0) {
      this.indicatorService.reassignNode(moving as Dashboard | DashTreeSection, null);
    } else {
      // It is a little tedious to get the correct drop position in a MatTree. CDK puts the position in currentIndex but counts DomNodes
      // with CdkDrag while excluding the node being dragged while canculating the new index. It uses an entirely different logic for
      // calculating the previousIndex field!
      const workingList = this.getCurrentFlatList(moving);
      const predcessor = workingList[event.currentIndex - 1];
      const newParent = this.treeControl.isExpanded(predcessor as IndicatorTreeNode & FlattenedNode)
        ? predcessor
        : predcessor.parent as IndicatorTreeNode;

      if (this.equalsNodeOrParent(moving, newParent)) {
        return;
      }

      this.indicatorService.reassignNode(moving as Dashboard | DashTreeSection, newParent as DashTreeSection);
    }

  }

  private getCurrentFlatList(exclude: IndicatorTreeNode) {
    return this.addToFlatList(this.treeDataSource.data, exclude, []);
  }

  private addToFlatList(root: IndicatorTreeNode[], exclude: IndicatorTreeNode, initList: IndicatorTreeNode[]) {
    return root
      .reduce(
        (list, node) => {
          if (!exclude || node.id !== exclude.id) {
            list.push(node);
          }

          if (
            isDashTreeSection(node)
            && this.treeControl.isExpanded(node as DashTreeSection & DashTreeChild & FlattenedNode)
          ) {
            this.addToFlatList((node.subSections as (DashTreeSection & DashTreeChild)[]) || [], exclude, list);
            return list.concat((node.indicators as (Dashboard & DashTreeChild)[]) || []);
          }

          return list;
        },
        initList
      );
  }

  private equalsNodeOrParent(candidate: IndicatorTreeNode, parent: IndicatorTreeNode): boolean {
    return parent
      && (
        candidate.id === parent.id
        || parent.parent
        && this.equalsNodeOrParent(candidate, parent.parent as IndicatorTreeNode)
      );
  }
}
