import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import { DataAdminService } from '../../../core/services/data-admin.service';
import { FileInput } from 'ngx-material-file-input';
import { ImportType } from 'src/app/shared/models/ImportType';
import { ImportPreResultList } from 'src/app/shared/models/ImportPreResultList';
import { Subscription, Subject } from 'rxjs';
import { ImportStatus } from 'src/app/shared/models/ImportStatus';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DataMappingTableComponent } from '../data-mapping-table/data-mapping-table.component';
import { FileStructureObject} from 'src/app/shared/models/FileStructure';
import { ConfirmDialogComponent } from '../../dash-tree/confirm-dialog/confirm-dialog.component';
import { FileStructureProviderService } from 'src/app/core/services/file-structure-provider.service';

@Component({
  selector: 'sie-import-progress',
  templateUrl: './import-progress.component.html',
  styleUrls: ['./import-progress.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class ImportProgressComponent implements OnInit, OnDestroy {

  @Input() selectedType!: ImportType;
  @Input() selectedFile!: FileInput;
  @Output() stepperClosed = new EventEmitter();

  isImportCheckRunning = false;
  isImportCheckSucceeded = false;
  isForceImportChecked = false;
  importId!: string;

  importPreResultList!: ImportPreResultList;
  isImportPreResultCalculationRunning = false;

  importStatus!: ImportStatus;
  private subscriptions: Subscription[] = [];

  fileStructure!: FileStructureObject;

  private stopPolling = new Subject();


  constructor(private dataAdminService: DataAdminService, 
    public dialogService: MatDialog, private fileStructureProvider: FileStructureProviderService) {
  }

  ngOnInit(): void {
    const fileStructure = this.fileStructureProvider.getFileStructure(this.selectedType);
    this.fileStructure = {
      FileStructure: JSON.parse(JSON.stringify(fileStructure.FileStructure)),
      FileStructureFrench: JSON.parse(JSON.stringify(fileStructure.FileStructureFrench))
    };
    this.importCheck(true);
  }

  importCheck(checkFile: boolean) {
    this.isImportCheckRunning = true;
    this.subscriptions.push(this.dataAdminService.importCheck(this.selectedType?.name, this.selectedFile.files[0], this.fileStructure, checkFile)
      .subscribe((initialImportStatus: ImportStatus) => {
        this.importId = initialImportStatus.importId;
        this.dataAdminService.waitForCheckImport(this.selectedType.name, this.importId)
          .pipe(
            takeUntil(this.stopPolling)
          ).subscribe((res) => {
            this.importStatus = res;
            if (res.status === 'done' || res.status === 'skipped') {
              this.isImportCheckRunning = false;
              this.isImportCheckSucceeded = true;
              this.stopPolling.next();
            } else if (res.status === 'failed') {
              this.isImportCheckRunning = false;
              this.isImportCheckSucceeded = false;
              this.stopPolling.next();
            }
            // else running do nothing
          });
      }));
  }

  importPreProcess() {
    this.isImportPreResultCalculationRunning = true;
    this.subscriptions.push(this.dataAdminService.importPreProcess(this.selectedType?.name, this.importId).subscribe((res) => {
      this.importPreResultList = res;
      this.isImportPreResultCalculationRunning = false;
    }));
  }

  nextClicked(stepper: MatStepper) {
    // complete the current step
    stepper.selected.completed = true;
    // move to next step
    stepper.next();
  }

  openDataMappingDialog(): void {
    const dialogRef = this.dialogService.open(DataMappingTableComponent, {
      width: '500px', height: '450px',
      autoFocus: false,
      data: this.fileStructure
    });

    this.subscriptions.push(dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fileStructure = result;
        this.importCheck(true);
      }
      else {
        this.fileStructure = {
          FileStructure: JSON.parse(JSON.stringify(this.fileStructure.FileStructure)),
          FileStructureFrench: JSON.parse(JSON.stringify(this.fileStructure.FileStructureFrench))
        };
      }
    }));
  }

  openConfirmDialog(isChecked: boolean) {
    this.isForceImportChecked = isChecked;
    if (isChecked) {
      const dialogRef = this.dialogService
        .open(ConfirmDialogComponent, { data: { action: `forcer l'importation ` } });
      this.subscriptions.push(dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.importCheck(false);
        }
        else {
          this.isForceImportChecked = false;
        }
      }));
    }


  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }


}
