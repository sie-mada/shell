import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileStructureObject } from 'src/app/shared/models/FileStructure';

@Component({
  selector: 'sie-data-mapping-table',
  templateUrl: './data-mapping-table.component.html',
  styleUrls: ['./data-mapping-table.component.scss']
})
export class DataMappingTableComponent {

  constructor(public dialogRef: MatDialogRef<DataMappingTableComponent>, @Inject(MAT_DIALOG_DATA) public fileStructure: FileStructureObject) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
