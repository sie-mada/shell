import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataMappingTableComponent } from './data-mapping-table.component';

describe('DataMappingTableComponent', () => {
  let component: DataMappingTableComponent;
  let fixture: ComponentFixture<DataMappingTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataMappingTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataMappingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
