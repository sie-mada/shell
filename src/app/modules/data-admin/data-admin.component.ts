import { Component, OnInit, OnDestroy } from '@angular/core';
import { FileInput } from 'ngx-material-file-input';
import { DataAdminService } from '../../core/services/data-admin.service';
import { Subscription } from 'rxjs';
import { ImportType } from '../../shared/models/ImportType';
import { CanComponentDeactivate } from 'src/app/core/guards/can-deactivate.guard';
import { MatDialog } from '@angular/material/dialog';
import { CloseDialogComponent } from 'src/app/shared/components/close-dialog/close-dialog.component';

@Component({
  selector: 'sie-data-admin',
  templateUrl: './data-admin.component.html',
  styleUrls: ['./data-admin.component.scss']
})
export class DataAdminComponent implements OnInit, OnDestroy, CanComponentDeactivate {

  isCheckStarted = false;

  selectedType?: ImportType;
  selectedFile?: FileInput;

  importTypes?: ImportType[];

  private subscriptions: Subscription[] = [];

  constructor(
    private readonly dataService: DataAdminService, 
    public dialogService: MatDialog) { }

  ngOnInit() {
    this.subscriptions.push(this.dataService.getImporters().subscribe((res) => {
      this.importTypes = res.filter(el => parseInt(el.order) > 0).sort((a, b) => a.order.localeCompare(b.order));
    }));

  }

  async canDeactivate(): Promise<boolean> {
    if (this.isCheckStarted) {
      const result = await this.dialogService
        .open(CloseDialogComponent, { data: { action: `quitter la demande d'importation`, info: `Vous risquez d'interrompre cette operation.` } })
        .afterClosed().toPromise();
      
      if (result) {
        return true
      }
      return false;

    }
    return true;
  }
  openFileCheckView() {
    this.isCheckStarted = true;
  }

  closeStepper() {
    this.isCheckStarted = false;
    this.selectedFile = undefined;
    this.selectedType = undefined;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
