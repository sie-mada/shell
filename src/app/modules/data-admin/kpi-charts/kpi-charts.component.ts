import { Component, OnInit, Input } from '@angular/core';
import { Color } from 'ng2-charts';
import { ChartOptions } from 'chart.js';
import { ImportPreResultList } from 'src/app/shared/models/ImportPreResultList';

@Component({
  selector: 'sie-kpi-charts',
  templateUrl: './kpi-charts.component.html',
  styleUrls: ['./kpi-charts.component.scss']
})
export class KpiChartsComponent implements OnInit {

  @Input() importPreResultList!: ImportPreResultList;
  @Input() chartType!: string;

  public chartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          type: "time",
          time: {
            unit: "month",
            displayFormats: {
              day: "DD-MM-YYYY"
            }
          }
        }
      ]
    }
  };

  public chartOptionsProductionAnnuelle: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          type: "time",
          time: {
            unit: "year",
            displayFormats: {
              day: "DD-MM-YYYY"
            }
          }
        }
      ]
    }
  };

  public chartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
