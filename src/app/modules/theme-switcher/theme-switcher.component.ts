import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sie-theme-switcher',
  templateUrl: './theme-switcher.component.html',
  styleUrls: ['./theme-switcher.component.sass']
})
export class ThemeSwitcherComponent implements OnInit {

  // let's define default theme
  themeColor = 'dark-theme';

  constructor() { }

  ngOnInit() {
    this.setDefaultTheme();
  }

  setDefaultTheme() {

    // if theme is stored in storage - use it
    if (localStorage.getItem('pxTheme')) {

      //set theme color to one from storage
      const themeInStorage = localStorage.getItem('pxTheme');
      if (themeInStorage !== null) {
        this.themeColor = themeInStorage;
      }

      //add that class to body
      const body = document.getElementsByTagName('body')[0];
      body.classList.add(this.themeColor);
    }
  }

  themeSwitcher() {

    const body = document.getElementsByTagName('body')[0];
    body.classList.remove(this.themeColor);

    // switch theme
    (this.themeColor == 'light-theme') ? this.themeColor = 'dark-theme' : this.themeColor = 'light-theme';

    body.classList.add(this.themeColor);

    //save it to local storage

    localStorage.setItem('pxTheme', this.themeColor);
  }

}
