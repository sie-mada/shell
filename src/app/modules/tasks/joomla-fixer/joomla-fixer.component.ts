import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { JoomlaFixerService } from '../../../core/services/joomla-fixer.service';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'sie-joomla-fixer',
  templateUrl: './joomla-fixer.component.html',
  styleUrls: ['./joomla-fixer.component.scss']
})
export class JoomlaFixerComponent implements OnInit {

  @ViewChild('prefix', { static: true })
  prefixControl!: MatSelect;

  selectedPrefix?: string;
  domains: Record<string, boolean> = {};
  replacementDomain?: string;

  $prefixes?: Observable<string[]>;
  $domains?: Observable<string[]>;

  performing = false;

  constructor(
    private readonly snack: MatSnackBar,
    private readonly service: JoomlaFixerService
  ) { }

  ngOnInit() {
    this.$prefixes = this.service.getPrefixes();

    this.prefixControl.valueChange
      .forEach((newPrefix: string) => this.$domains = newPrefix ? this.service.getDomains(newPrefix) : undefined);
  }

  performReplacement() {
    if (!this.selectedPrefix || !this.replacementDomain) {
      return;
    }

    this.performing = true;

    const domains = Object.keys(this.domains)
      .filter(domain => this.domains[domain]);

    this.service
      .replaceDomains(this.selectedPrefix, domains, this.replacementDomain)
      .subscribe({
        next: changes => {
          this.performing = false;
          this.selectedPrefix = undefined;

          this.snack.open(`${changes} adresses ont été changées`, undefined, { duration: 5000 });
        },
        error: e => {
          this.performing = false;
          console.error(e);

          this.snack.open('Le changement des adresses a échoué', undefined, { duration: 5000 });
        }
      });
  }
}
