import { Component, OnInit, OnDestroy } from '@angular/core';
import { ImportHistoryElement } from 'src/app/shared/models/ImportHistoryElement';
import { DataAdminService } from 'src/app/core/services/data-admin.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'sie-imports-management',
  templateUrl: './imports-management.component.html',
  styleUrls: ['./imports-management.component.scss']
})
export class ImportsManagementComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['dateStarted', 'importStep', 'importId', 'importStatus', 'userName', 'importType'];
  dataSource: ImportHistoryElement[] = [];
  private subscriptions: Subscription[] = [];

  constructor(private readonly dataAdminService: DataAdminService) { }

  ngOnInit(): void {
    this.subscriptions.push(this.dataAdminService.getImportHistoryList().subscribe((res: ImportHistoryElement[]) => {
      this.dataSource = res.filter(el => el.importStep !== 'import');
    }));
  }

  showHistory(isChecked: boolean) {
    this.subscriptions.push(this.dataAdminService.getImportHistoryList().subscribe((res: ImportHistoryElement[]) => {
      if (isChecked) {
        this.dataSource = res;
      }
      else {
        this.dataSource = res.filter(el => el.importStep !== 'import');
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}

