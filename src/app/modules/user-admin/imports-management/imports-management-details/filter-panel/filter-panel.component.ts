import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ImportPreResultList, ImportPreResultElement } from 'src/app/shared/models/ImportPreResultList';

@Component({
  selector: 'sie-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.scss']
})
export class FilterPanelComponent implements OnInit {
  @Input() initialMinYearValue!: string;
  @Input() initialMaxYearValue!: string;
  @Input() elements!: ImportPreResultList;
  @Output() selectionChanged = new EventEmitter<ImportPreResultElement>();
  @Output() minFilterChanged = new EventEmitter<string>();
  @Output() maxFilterChanged = new EventEmitter<string>();
  @Output() filterRemoved = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

}
