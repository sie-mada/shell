import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectImportDialogComponent } from './reject-import-dialog.component';

describe('RejectImportDialogComponent', () => {
  let component: RejectImportDialogComponent;
  let fixture: ComponentFixture<RejectImportDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectImportDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectImportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
