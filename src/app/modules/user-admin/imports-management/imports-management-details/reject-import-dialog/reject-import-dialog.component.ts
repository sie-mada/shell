import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'sie-reject-import-dialog',
  templateUrl: './reject-import-dialog.component.html',
  styleUrls: ['./reject-import-dialog.component.scss']
})
export class RejectImportDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RejectImportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
