import { Component, OnInit, OnDestroy } from '@angular/core';
import { ImportPreResultList, ImportPreResultElement } from 'src/app/shared/models/ImportPreResultList';
import { ActivatedRoute } from '@angular/router';
import { DataAdminService } from 'src/app/core/services/data-admin.service';
import { ImportHistoryElement } from 'src/app/shared/models/ImportHistoryElement';
import { Subscription } from 'rxjs';
import { ImportDetail } from 'src/app/shared/models/ImportDetail';
import { FileStructureViewerComponent } from './file-structure-viewer/file-structure-viewer.component';
import { MatDialog } from '@angular/material/dialog';
import { FileStructureObject } from 'src/app/shared/models/FileStructure';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RejectImportDialogComponent } from './reject-import-dialog/reject-import-dialog.component';
import { RejectInfoList } from 'src/app/shared/models/RejectInfoList';
import { RejectInfoHistoryViewerComponent } from './reject-info-history-viewer/reject-info-history-viewer.component';
import { CanComponentDeactivate } from 'src/app/core/guards/can-deactivate.guard';
import { CloseDialogComponent } from 'src/app/shared/components/close-dialog/close-dialog.component';

@Component({
  selector: 'sie-imports-management-details',
  templateUrl: './imports-management-details.component.html',
  styleUrls: ['./imports-management-details.component.scss']
})
export class ImportsManagementDetailsComponent implements OnInit, OnDestroy, CanComponentDeactivate {

  importing = false;
  isImportSucceeded!: boolean;

  importStep!: string;

  importPreResultList!: ImportPreResultList;
  downloadUrl!: string;
  selectedImportElement?: ImportHistoryElement;
  fileStructure!: FileStructureObject;
  rejectInfoList!: RejectInfoList;


  selectedChartType = 'bar';
  initialMinYearValue = '';
  initialMaxYearValue = '';

  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
    private readonly dataAdminService: DataAdminService,
    public dialogService: MatDialog, private readonly snack: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.subscriptions.push(this.route.params.subscribe(params => {
      const importId = params['id'];
      const importStep = params['step'];
      this.importStep = importStep;

      this.subscriptions.push(this.dataAdminService.getImportHistoryList().subscribe((res: ImportHistoryElement[]) => {
        this.selectedImportElement = res.find(el => (el.importId === importId) && (el.importStep === importStep));
        // Set download link
        this.subscriptions.push(this.dataAdminService.getCoreConfig().subscribe((config) => {
          this.downloadUrl = `${config.shell}/data/importer/${this.selectedImportElement?.type}/download/${this.selectedImportElement?.importId}`;
        }));
        this.getImportDetails();
      }));
    }));
  }

  async canDeactivate(): Promise<boolean> {
    if (this.importing) {
      const result = await this.dialogService
      .open(CloseDialogComponent, { data: { action: `quitter l'importation`, info: `Vous risquez d'interrompre cette operation.` } })
      .afterClosed().toPromise();
      
      if (result) {
        return true
      }
      return false;

    }
    return true;
  }

  download() {
    location.href = this.downloadUrl;
  }

  private getImportDetails() {
    this.subscriptions.push(this.dataAdminService.getImportDetail(this.selectedImportElement!.type, this.selectedImportElement!.importId).subscribe((importDetail: ImportDetail) => {
      this.importPreResultList = importDetail?.kpis;
      this.fileStructure = JSON.parse(importDetail.fileStructure);
      this.rejectInfoList = JSON.parse(importDetail.rejectInfoList);
    }));
  }

  upload() {
    if (!this.selectedImportElement?.type || !this.selectedImportElement.filename) {
      return;
    }

    this.importing = true;
    this.dataAdminService
      .upload(this.selectedImportElement?.type, this.selectedImportElement?.importId)
      .pipe(

      )
      .subscribe(
        () => {
          this.importing = false;
          this.isImportSucceeded = true;
          this.snack.open('L\'importation a été approuvée et les données ont été intégrées dans la base de données et peuvent être consultées par les utilisateurs', 'Fermer');
        },
        (e: any) => {
          console.error(e);
          this.importing = false;
          this.isImportSucceeded = false;
          this.snack.open('L\'importation a échoué! Contactez un administrateur pour l\'assistance.', 'Fermer');
        }
      );
  }

  openFileStructureViewerDialog(): void {
    this.dialogService.open(FileStructureViewerComponent, {
      width: '600px', height: '500px',
      autoFocus: false,
      data: this.fileStructure?.FileStructureFrench
    });
  }

  openRejectInfoHistoryViewerDialog(): void {
    this.dialogService.open(RejectInfoHistoryViewerComponent, {
      width: '600px', height: '500px',
      autoFocus: false,
      data: this.rejectInfoList
    });
  }

  openRejectImportDialog(): void {
    const dialogRef = this.dialogService.open(RejectImportDialogComponent, {
      width: '500px', height: '300px',
      data: { rejectText: '' }
    });

    this.subscriptions.push(dialogRef.afterClosed().subscribe((rejectText: string) => {
      if (rejectText) {
        if (!this.selectedImportElement?.type) {
          return;
        }
        this.subscriptions.push(this.dataAdminService.rejectImport(this.selectedImportElement?.type, this.selectedImportElement?.importId, rejectText)
          .subscribe(() => {
            // Update importDetails after triggering a change
            this.getImportDetails();
          }
          ));
      }
    }));
  }

  minFilter(value: string) {
    const tempImportPreResultList: ImportPreResultList = [];
    if (!!value && value.toString().length === 4) {
      this.importPreResultList.forEach(element => {
        let tempImportPreResultElement: ImportPreResultElement = element;

        const toRemoveElements = element.commonLabels.filter(el => el?.split('-')[0] < value);
        toRemoveElements?.forEach(ele => {
          const index = element.commonLabels?.indexOf(ele);
          if (index > -1) {
            element.commonLabels?.splice(index, 1);
            // filter oldValues
            element.oldValues?.data.splice(index, 1);
            // filter newValues
            element.newValues?.data.splice(index, 1);
          }

        });
        tempImportPreResultElement = element;
        tempImportPreResultList.push(tempImportPreResultElement);
      });
      this.importPreResultList = tempImportPreResultList;
    }
  }

  maxFilter(value: string) {
    const tempImportPreResultList: ImportPreResultList = [];
    if (!!value && value.toString().length === 4) {
      this.importPreResultList.forEach(element => {
        let tempImportPreResultElement: ImportPreResultElement = element;

        const toRemoveElements = element.commonLabels.filter(el => el?.split('-')[0] > value);
        toRemoveElements?.forEach(ele => {
          const index = element.commonLabels?.indexOf(ele);
          if (index > -1) {
            element.commonLabels?.splice(index, 1);
            // filter oldValues 
            element.oldValues?.data.splice(index, 1);
            // filter newValues
            element.newValues?.data.splice(index, 1);
          }

        });
        tempImportPreResultElement = element;
        tempImportPreResultList.push(tempImportPreResultElement);
      });
      this.importPreResultList = tempImportPreResultList;
    }
  }

  onChange(event: ImportPreResultElement) {
    this.importPreResultList = this.importPreResultList.filter(el => el.kpiName === event.kpiName);
  }

  removeFilter() {
    // Get default list of kpis without filter
    this.subscriptions.push(this.dataAdminService.getImportDetail(this.selectedImportElement!.type, this.selectedImportElement!.importId).subscribe((importDetail: ImportDetail) => {
      this.importPreResultList = importDetail.kpis;
      this.initialMinYearValue = '0';
      this.initialMaxYearValue = '0';
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
