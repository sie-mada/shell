import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileStructureObject } from 'src/app/shared/models/FileStructure';

@Component({
  selector: 'sie-file-structure-viewer',
  templateUrl: './file-structure-viewer.component.html',
  styleUrls: ['./file-structure-viewer.component.scss']
})
export class FileStructureViewerComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<FileStructureViewerComponent>, @Inject(MAT_DIALOG_DATA) public fileStructure: FileStructureObject) { }

  ngOnInit(): void {
  }

}
