import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RejectInfoList } from 'src/app/shared/models/RejectInfoList';

@Component({
  selector: 'sie-reject-info-history-viewer',
  templateUrl: './reject-info-history-viewer.component.html',
  styleUrls: ['./reject-info-history-viewer.component.scss']
})
export class RejectInfoHistoryViewerComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RejectInfoHistoryViewerComponent>, @Inject(MAT_DIALOG_DATA) public rejectInfoList: RejectInfoList) { }

  ngOnInit(): void {
  }

}
