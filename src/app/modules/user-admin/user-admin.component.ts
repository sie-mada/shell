import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../dash-tree/confirm-dialog/confirm-dialog.component';
import { filter, flatMap } from 'rxjs/operators';
import { User } from '../../shared/models/User';

@Component({
  selector: 'sie-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements OnInit {

  $users!: Observable<User[]>;

  constructor(
    private readonly dialog: MatDialog,
    private readonly userService: UserService
  ) { }

  ngOnInit() {
    this.updateUserList();
  }

  formatUser(user: User) {
    const realName = user.firstName || user.lastName
      ? `${user.firstName || ''} ${user.lastName || ''}`.trim()
      : null;

    return realName
      ? `${realName} (${user.username})`
      : user.username;
  }

  delete(username: string) {
    this.dialog
      .open(ConfirmDialogComponent, { data: { action: `supprimer le compte d'utilisateur « ${username} »` } })
      .afterClosed()
      .pipe(
        filter(Boolean),
        flatMap(() => this.userService.delete(username))
      )
      .subscribe(() => this.updateUserList());
  }

  private updateUserList() {
    this.$users = this.userService.listUsers();
  }
}
