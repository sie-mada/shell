import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/User';

@Component({
  selector: 'sie-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  user: Partial<User> = {};
  newPassword?: string;
  repeatPassword?: string;
  $allGroups!: Observable<string[]>;
  isExistingUser = true;
  saving = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly snakBar: MatSnackBar,
    private readonly userService: UserService
  ) { }

  ngOnInit() {
    this.$allGroups = this.userService.listGroups();

    this.route.paramMap
      .subscribe(
        params => {
          const username = params.get('username');
          if (username === '~nouveau') {
            this.isExistingUser = false;
          } else if (username) {
            this.isExistingUser = true;
            this.loadUser(username);
          }
        }
      );
  }

  loadUser(username: string) {
    this.userService
      .getUserDetails(username)
      .subscribe(user => this.user = user);
  }

  save() {
    this.saving = true;

    // tslint:disable: no-non-null-assertion
    const saveObservable = this.isExistingUser
      ? this.userService.update(
          {
            username: this.user.username!,
            isAuthenticated: false,
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            mail: this.user.mail,
            groups: [...(this.user.groups || [])]
          },
          this.newPassword
        )
      : this.userService.add(
          { ...(this.user as User) },
          this.newPassword!
        );
    // tslint:enable: no-non-null-assertion

    saveObservable.subscribe(
      () => this.router.navigate(['..'], { relativeTo: this.route }),
      err => (console.error(err), this.saving = false, this.snakBar.open('Sauvegarde échouée', undefined, { duration: 5000 })),
      () => this.saving = false
    );
  }
}
