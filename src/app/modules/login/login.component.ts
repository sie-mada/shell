import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { Subscription, EMPTY } from 'rxjs';
import { take, catchError, finalize } from 'rxjs/operators';
import { MatInput } from '@angular/material/input';

@Component({
  selector: 'sie-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('usernameInput')
  usernameInput!: MatInput;

  username?: string;
  password?: string;
  loggingIn = false;
  errorMessage?: string;

  private userSubscription?: Subscription;

  constructor(
    private readonly changeDetector: ChangeDetectorRef,
    private readonly router: Router,
    private readonly userService: UserService
  ) { }

  ngOnInit() {
    this.userSubscription = this.userService.user
      .subscribe(user => {
        if (user.isAuthenticated) {
          this.router.navigate(['/accueil']);
        }
      });
  }

  ngAfterViewInit() {
    this.usernameInput.focus();
    this.changeDetector.detectChanges();;
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  tryLogin() {
    if (!this.username || !this.password) {
      return;
    }

    this.loggingIn = true;
    this.userService
      .login(this.username, this.password)
      .pipe(
        catchError(error => {
          console.error('Login failed', error);
          this.errorMessage = 'Nom d\'utilisateur ou mot de passe invalide';
          this.password = '';
          return EMPTY;
        }),
        take(1),
        finalize(() => this.loggingIn = false)
      )
      .subscribe();
  }
}
