import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { UserService } from '../../core/services/user.service';

@Directive({
  selector: '[sieAuthorize]'
})
export class AuthorizeDirective {

  private hasView = false;

  constructor(
    private readonly userService: UserService,
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef
  ) { }

  @Input()
  set sieAuthorize(groups: string | string[]) {
    const parsedGroups = Array.isArray(groups)
      ? groups
      : groups.split(',');

    this.userService.user
      .subscribe(
        user => {
          if (
            !this.hasView
            && user.isAuthenticated
            && user.groups
            && user.groups.find(group => parsedGroups.includes(group))
          ) {
            this.viewContainer.createEmbeddedView(this.templateRef);
            this.hasView = true;
          } else if (this.hasView) {
            this.viewContainer.clear();
            this.hasView = false;
          }
        }
      );
  }
}
