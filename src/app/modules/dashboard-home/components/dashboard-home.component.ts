import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashTreeService } from '../../../core/services/dash-tree.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AddSectionDialogComponent } from './add-section-dialog/add-section-dialog.component';

@Component({
  selector: 'sie-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit, OnDestroy {

  dashboardSections!: any[];
  defaultElevation = 2;
  raisedElevation = 8;
  private subscriptions: Subscription[] = [];

  constructor(private readonly dashTreeService: DashTreeService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.subscriptions.push(this.dashTreeService.treeChanged$.subscribe(data => {
      this.dashboardSections = data;
    }));
  }

  openAddSectionDialog(): void {
    const dialogRef = this.dialog.open(AddSectionDialogComponent, {
      width: '300px',
      data: { sectionName: '' }
    });

    this.subscriptions.push(dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.dashTreeService.addSection(undefined, result);
      }
    }));
  }

  deleteSection(sectionName: string): void {
    this.dashTreeService.deleteSection(sectionName);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
