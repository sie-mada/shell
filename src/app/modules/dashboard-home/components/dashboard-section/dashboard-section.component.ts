import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashTreeService } from 'src/app/core/services/dash-tree.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Dashboard } from 'src/app/shared/models/Dashboard';

@Component({
  selector: 'sie-dashboard-section',
  templateUrl: './dashboard-section.component.html',
  styleUrls: ['./dashboard-section.component.scss']
})
export class DashboardSectionComponent implements OnInit, OnDestroy {

  indicators!: Dashboard[];
  selectedSectionId!: string;
  defaultElevation = 2;
  raisedElevation = 8;
  showOverlay = false;
  hoveredIndicatorId!: string;
  private subscriptions: Subscription[] = [];

  constructor(private readonly dashTreeService: DashTreeService, private route: ActivatedRoute){ }

  ngOnInit(): void {
    this.subscriptions.push(this.route.params.subscribe(params => {
      this.selectedSectionId = params['sec'];
      this.subscriptions.push(this.dashTreeService.treeChanged$.subscribe(data => {
        const currentSection = data.find(sec => sec.id === this.selectedSectionId)
        if(currentSection){
          if(currentSection.indicators){
            this.indicators = currentSection.indicators;
          }
        }
      }));
    }));
  
  }

  onShowOverlay(event: boolean, id: string){
    this.showOverlay = event;
    if(event === true){
      this.hoveredIndicatorId = id;
    }
    else {
      this.hoveredIndicatorId = undefined as unknown as '';
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
