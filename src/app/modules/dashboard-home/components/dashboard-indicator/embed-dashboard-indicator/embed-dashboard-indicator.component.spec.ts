import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbedDashboardIndicatorComponent } from './embed-dashboard-indicator.component';

describe('EmbedDashboardIndicatorComponent', () => {
  let component: EmbedDashboardIndicatorComponent;
  let fixture: ComponentFixture<EmbedDashboardIndicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbedDashboardIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbedDashboardIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
