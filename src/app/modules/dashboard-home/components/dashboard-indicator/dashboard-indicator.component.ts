import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DashTreeService } from 'src/app/core/services/dash-tree.service';

@Component({
  selector: 'sie-dashboard-indicator',
  templateUrl: './dashboard-indicator.component.html',
  styleUrls: ['./dashboard-indicator.component.scss']
})
export class DashboardIndicatorComponent implements OnInit, OnDestroy {

  selectedIndicatorUrl!: string;
  private subscriptions: Subscription[] = [];

  constructor(private readonly dashTreeService: DashTreeService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscriptions.push(this.route.params.subscribe(params => {
      const selectedSectionId = params['sec'];
      const selectedIndicatorId = params['dash'];

      this.subscriptions.push(this.dashTreeService.treeChanged$.subscribe(data => {
        const currentSection = data.find(sec => sec.id === selectedSectionId)
        if (currentSection && currentSection.indicators) {
          const currentIndicator = currentSection.indicators.find(ind => ind.id === selectedIndicatorId);
          if (currentIndicator && currentIndicator.url) {
            this.selectedIndicatorUrl = currentIndicator.url;
          }
        }
      }));
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub=> sub.unsubscribe());
  }
}
