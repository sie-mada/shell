import { Moment } from 'moment';

export interface Comment {
    user: string;
    timestamp: Moment;
    content: string;
  }