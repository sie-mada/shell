export type RejectInfoList = RejectInfoElement[];

export interface RejectInfoElement {
    message?: string;
    userName?: string;
}