import { Dashboard } from './Dashboard';

export interface DashControlsData {
    dashboard: Dashboard;
    url: string;
  }