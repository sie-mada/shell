export interface Dashboard {
    id: string;
    title: string;
    adapter: string;
    remoteId: string | number;
    url: string;
    weight?: number;
}

export interface DashTreeSection {
    id: string;
    name: string;
    title: string;
    weight?: number;

    subSections?: DashTreeSection[];
    indicators?: Dashboard[];
  }

  export interface DashTreeChild {
    parent: DashTreeSection;
  }

  export function isDashboard(candidate: unknown): candidate is Dashboard {
    return !!candidate
      && typeof candidate === 'object'
      && typeof (candidate as Dashboard).adapter === 'string'
      && typeof (candidate as Dashboard).title === 'string'
      && typeof (candidate as Dashboard).url === 'string';
  }

  export function isDashTreeSection(candidate: unknown): candidate is DashTreeSection {
    return !!candidate
      && typeof candidate === 'object'
      && typeof (candidate as DashTreeSection).name === 'string'
      && typeof (candidate as DashTreeSection).title === 'string';
  }
