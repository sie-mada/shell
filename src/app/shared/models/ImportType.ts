export interface ImportType {
    name: string;
    label: string;
    description: string;
    example?: string;
    fileTypes?: string;
    order: string;
  }