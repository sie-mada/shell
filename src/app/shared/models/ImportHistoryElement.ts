export interface ImportHistoryElement {
    importId: string;
    dateStarted: string;
    importStep: string;
    importStatus: 'running' | 'done' | 'failed';
    userName: string;
    type: string;
    filename: string;
}