export interface ImportError {
  errorType?: string;
  errorDescription?: string;
}

export interface ImportStatus {
  importId: string;
  status: 'running' | 'skipped' | 'done' | 'failed';
  start: string;
  end?: string;
  result?: string;
  // errors?: string;
  importErrors: ImportError[];
}