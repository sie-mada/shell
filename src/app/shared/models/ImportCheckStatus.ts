export interface ImportCheckStatus {
    status: 'Ok' | 'Error'
    errorInformation: any
}