import { Moment } from 'moment';

export interface DashboardComment {
    adapter: string;
    remoteId: string;
    user: string;
    timestamp: Moment;
    content: string;
  }