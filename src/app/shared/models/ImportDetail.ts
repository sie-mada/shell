import { ImportPreResultList } from './ImportPreResultList';

export interface ImportDetail {
    kpis: ImportPreResultList;
    fileStructure: string;
    rejectInfoList: string;
}