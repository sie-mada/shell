import { User } from './User';

export interface UserSessionRequestMessage {
    key: 'userSession';
    session: User;
}

export function isUserSessionRequestMessage(candidate: any): candidate is UserSessionRequestMessage {
    return !!candidate
        && typeof candidate === 'object'
        && candidate.key === 'userSession'
        && !!candidate.session
        && typeof candidate.session === 'object';
}