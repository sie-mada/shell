import { DashTreeSection, Dashboard, DashTreeChild } from './Dashboard';

export interface IndicatorTreeAppender {
    type: 'sectionAppender';
    id: string;
  }
  
  export function isAppender(candidate: any): candidate is IndicatorTreeAppender {
    return !!candidate
      && typeof candidate === 'object'
      && candidate.type === 'sectionAppender';
  }
  
  export type IndicatorTreeNode = (DashTreeSection | Dashboard | IndicatorTreeAppender) & DashTreeChild;
  
  export interface FlattenedNode {
    level: number;
  }