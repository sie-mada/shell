export type ImportPreResultList = ImportPreResultElement[];

export interface ImportPreResultElement {
  kpiName: string;
  oldValues: DataSet;
  newValues: DataSet;
  oldLabels: string[];
  newLabels: string[];
  commonLabels: string[];
}

interface DataSet {
  data: number[],
  label: string,
}