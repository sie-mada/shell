export interface User {
    username?: string;
    password?: string;
    groups?: string[];
    firstName?: string;
    lastName?: string;
    mail?: string;
    isAuthenticated: boolean;
  }