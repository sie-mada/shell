export interface CloseDialogData {
    action: string;
    info: string;
}